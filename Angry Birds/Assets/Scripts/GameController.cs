﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameController : MonoBehaviour
{
    // Fungsi Singleton, ditambahkan untuk si OwlBird
    private static GameController _instance = null;
    public static GameController Instance

    {
        get
        {

            if (_instance == null)

            {

                _instance = FindObjectOfType<GameController>();

            }
            return _instance;

        }
    }

    public SlingShooter SlingShooter;
    public TrailController TrailController;
    public List<Bird> Birds;
    public List<Enemy> Enemies;
    private int enemyCount;
    
    private Bird _shotBird;
    public BoxCollider2D TapCollider;

    private bool _isGameEnded = false;


    void Start()
    {
        enemyCount = Enemies.Count;
        for (int i = 0; i < Birds.Count; i++)
        {
            Birds[i].OnBirdDestroyed += ChangeBird;
            Birds[i].OnBirdShot += AssignTrail;
        }

        for (int i = 0; i < Enemies.Count; i++)
        {
            Enemies[i].OnEnemyDestroyed += CheckGameEnd;
        }

        TapCollider.enabled = false;
        SlingShooter.InitiateBird(Birds[0]);
        _shotBird = Birds[0];
    }

    public void ChangeBird()
    {
        TapCollider.enabled = false;

        if (_isGameEnded)
        {
            return;
        }

        Birds.RemoveAt(0);

        if (Birds.Count > 0)
        {
            SlingShooter.InitiateBird(Birds[0]);
            _shotBird = Birds[0];
        }
            
    }

    public void AssignTrail(Bird bird)
    {
        TrailController.SetBird(bird);
        StartCoroutine(TrailController.SpawnTrail());
        TapCollider.enabled = true;
    }

    void OnMouseUp()
    {
        if (_shotBird != null)
        {
            _shotBird.OnTap();
        }
    }

    public void CheckGameEnd(GameObject destroyedEnemy)
    {
        for (int i = 0; i < Enemies.Count; i++)
        {
            if (Enemies[i].gameObject == destroyedEnemy)
            {
                Enemies.RemoveAt(i);
                enemyCount -= 1;
                break;
            }
               
        }
    }
    
    //untuk end condition, tapi sebenarnya looping hehe
    public IEnumerator Endgame()
    {
        if (enemyCount == 0)
        {
            _isGameEnded = true;
        }

        if ((enemyCount > 0) && (Birds.Count <= 0))
        {
            yield return new WaitForSeconds(2f);
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }

        if (_isGameEnded)
        {
            yield return new WaitForSeconds(2f);
            Scene scene = SceneManager.GetActiveScene();
            if (scene.name == "Gameplay")
                SceneManager.LoadScene("Gameplay2");
            else if (scene.name == "Gameplay2")
                SceneManager.LoadScene("Gameplay");
        }
    }

    //Diambil dan dimodifikasi berdasarkan materi game 2D Tower Defense, diambil splash radiusnya untuk destroy object.
    public void ExplodeAt(Vector2 point, float radius)
    {
        foreach (Enemy enemy in Enemies)
        {
            if (enemy.gameObject.activeSelf)
            {
                if (Vector2.Distance(enemy.transform.position, point) <= radius)
                {
                    enemy.OnEnemyDestroyed(gameObject);
                    enemyCount -= 1;
                    Destroy(enemy.gameObject);
                }
            }
        }
    }

    private void Update()
    {
        StartCoroutine(Endgame());
    }
}
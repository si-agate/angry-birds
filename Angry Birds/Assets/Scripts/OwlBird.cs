﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OwlBird : Bird
{
    public GameObject explodeEffect;
       
    //hanya untuk enable particle effect dan destroy OwlBird
    void onExplode()
    {
        if (State == BirdState.HitSomething)
        {
            Instantiate(explodeEffect, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
    
    
    // Update is called once per frame
    void Update()
    {
        onExplode();
    }
}
